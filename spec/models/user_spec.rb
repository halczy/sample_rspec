require 'rails_helper'

RSpec.describe User, type: :model do
  it 'is valid with a first name, last name, eamil, and password' do
    user = User.new(first_name: 'Ziyang',
                    last_name:  'Chen',
                    email:     'user_model@rspec.test',
                    password:  'example')
    expect(user).to be_valid
  end
  
  it 'is invalid without a first name' do
    user = User.new(first_name: nil)
    user.valid?
    expect(user.errors[:first_name]).to include("can't be blank")
  end
  
  it 'is invlaid without a last name' do
    user = User.new(last_name: nil)
    user.valid?
    expect(user.errors[:first_name]).to include("can't be blank")
  end
  
  it 'is invalid without a eamil' do
    user = User.new(email: nil)
    user.valid?
    expect(user.errors[:email]).to include("can't be blank")
      
  end
  
  it 'is invlaid with a duplicate email address' do
    User.create(first_name: 'John',
             last_name:  'Doe',
             email:     'john_doe@rspec.test',
             password:  'example')
             
    user = User.create(first_name: 'Jane',
                    last_name:  'Doe',
                    email:     'john_doe@rspec.test',
                    password:  'example')
    user.valid?
    expect(user.errors[:email]).to include('has already been taken')
  end
  
  it "returns a user's full name as a string" do
    user = User.new(first_name: 'John',
                    last_name:  'Doe',
                    email: 'johndoe@example.com')
    expect(user.name).to eq('John Doe')
  end
end
