class Project < ApplicationRecord
  # Relationships
  belongs_to :user
  has_many :notes
  
  # Validations
  validates :name, presence: true, uniqueness: { scope: :user_id }
  
end
