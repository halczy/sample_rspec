class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  # Relationships
  has_many :projects
  has_many :notes
  
  # Validations
  validates :first_name, :last_name, presence: true
  
  
  def name
    [first_name, last_name].join(' ')
  end
end
