class Note < ApplicationRecord
  # Relationships
  belongs_to :user
  belongs_to :project
  
  # Validations
  validates :message, presence: true
  
  # Scopes  
  scope :search, ->(search_term) {
    where("message LIKE ?", "%#{search_term}%")
  }
end
